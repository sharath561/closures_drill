const cacheFunction = require("../cacheFunction");

function callBack(data, cache) {
  cache[data] = 1;
  return [data, cache[data]];
}

const invoke = cacheFunction(callBack);

console.log(invoke([1, 2]));
console.log(invoke([1, 2]));
console.log(invoke("sharath"));
console.log(invoke("batman"));
console.log(invoke("bruce wayne"));
console.log(invoke("batman"));
