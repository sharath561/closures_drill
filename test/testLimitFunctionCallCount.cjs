const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

function callBack(number) {
  number++;
  return number;
}

const callTimes = 5;

const invoker = limitFunctionCallCount(callBack, callTimes);

for (let item = 0; item <= 10; item++) {
  console.log(invoker());
}
