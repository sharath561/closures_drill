const mainFunction = require("../counterFactory.cjs");
const invoker = mainFunction();

console.log(invoker.increment());
console.log(invoker.increment());
console.log(invoker.decrement());
console.log(invoker.decrement());
