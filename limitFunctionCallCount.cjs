function limitFunctionCallCount(cb, n) {
  if (typeof cb == "function" && typeof n == "number") {
    let count = 0;
    function invoke() {
      if (count < n) {
        count = cb(count);
        return `callBack is called ${count} time`;
      } else {
        return null;
      }
    }
    return invoke;
  } else {
    return `something went wrong`;
  }
}

module.exports = limitFunctionCallCount;
