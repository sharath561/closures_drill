function cacheFunction(cb) {
  if (typeof cb == "function") {
    const cache = {};

    function invoke(...args) {
      if (cache[String(...args)]) {
        cache[String(...args)] += 1;
        return [String(...args), cache[String(...args)]];
      } else {
        const result = cb(String(...args), cache);
        return result;
      }
    }
    return invoke;
  } else {
    return "something error occured";
  }
}

module.exports = cacheFunction;
